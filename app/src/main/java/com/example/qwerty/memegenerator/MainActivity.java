package com.example.qwerty.memegenerator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button done;
    private EditText top, bot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        done = (Button)findViewById(R.id.btnDone);
        top = (EditText)findViewById(R.id.etTop);
        bot = (EditText)findViewById(R.id.etBot);
        done.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;

                switch(view.getId())
                {
                    case R.id.btnDone:
                        intent = new Intent(this, Main2Activity.class);
                        if(TextUtils.isEmpty(top.getText().toString()) && TextUtils.isEmpty(bot.getText().toString()))
                            Toast.makeText(MainActivity.this, "at least fill up one field!", Toast.LENGTH_SHORT).show();
                        else{
                        intent.putExtra("top", top.getText().toString());
                        intent.putExtra("bot", bot.getText().toString());
                        startActivity(intent);}

                }
    }
}
