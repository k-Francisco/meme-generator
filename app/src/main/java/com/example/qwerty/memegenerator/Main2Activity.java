package com.example.qwerty.memegenerator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    private TextView top, bot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        top = (TextView)findViewById(R.id.tvTopText);
        bot = (TextView)findViewById(R.id.tvBotText);
        top.setText(intent.getStringExtra("top"));
        bot.setText(intent.getStringExtra("bot"));
    }
}
